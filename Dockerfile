FROM node:10

RUN mkdir /app
WORKDIR /app

# Install external dependencies here

# App source
COPY . .
RUN npm ci --only=production
RUN cd app && npm ci only=production

# Run app
EXPOSE 3000
CMD [ "npm", "start" ]
