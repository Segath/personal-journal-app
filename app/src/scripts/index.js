import "../styles/index.scss";
import * as moment from "moment";

const journalPath = "/public/journal.html";
const loginPath = "/public/login.html";
const registerPath = "/public/register.html";

// Run this when the document is ready
$(document).ready(() => {
	redirectToFile(journalPath);
	getAllJournalEntries();
});

// Render all articles onto the journal page
function renderArticles(articles) {
	$("#row").empty();
	$.each(articles, (i, post) => {
		$("#row").append(createArticle(post));
	});
}

// Create all the articles
function createArticle(post) {
	return `
	<article class="col-md-12" id="${post.id}" style="padding:10px">
        <h3>${post.title}</h3>

        <h4>${post.subTitle}</h4>
		<div> Created: ${post.createdDate}</div>

        <div class ="article-body">
            <p>${post.content}</p>
        </div>

        <div>
           Last modified: ${post.modifiedDate}
		</div>

		<button class="btn btn-primary" data-toggle="modal" data-target="#myModal" onclick="getAJournalEntry(${post.id})">UPDATE</button>
		<button class="btn btn-danger" onclick="deletePost(${post.id})">DELETE</button>
	</article>
    `;
}

// Fetch data from the form and send that data to the API.
function createPost() {
	var titleInput = $('input[name="title"]')
		.val()
		.trim();
	var subtitleInput = $('input[name="subtitle"]')
		.val()
		.trim();
	var contentInput = $('textarea[name="content"]')
		.val()
		.trim();
	if (titleInput && subtitleInput && contentInput) {
		var post = {
			title: titleInput,
			subTitle: subtitleInput,
			createdDate: moment().format("ddd H:mm, DD-MM-YYYY"),
			content: contentInput,
			modifiedDate: moment().format("ddd H:mm, DD-MM-YYYY")
		};
		postAJournalEntry(post);
	} else {
		alert("All fields must have a valid value.");
	}
}

//Display the modal on the page, which will allow the user to update a post
function editPost(post) {
	$(".modal-body").empty().append(`
            <form id="updatePost" action="">
                <div class="form-group">
                <label for="title">Title</label>
                <input class="form-control" id="mtitle" "type="text" name="title" value="${post.title}"/>
                <label for="subtitle">Subtitle</label>
				<input class="form-control" id="msubtitle" type="text" name="subtitle" value="${post.subTitle}"/>
                <label for="content">Content</label>
                <textarea class="form-control" id="mcontent" type="text" name="content"">${post.content}</textarea>
                </div>
        `);
	$(".modal-footer").empty().append(`
                <button type="button" type="submit" class="btn btn-primary" onClick="updatePost(${post.id})">Save changes</button>
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
            </form>
    `);
}

// Helper function to display a confirmation box and then delete the post
function deletePost(id) {
	var action = confirm("Are you sure you want to delete this post?");
	if (action == true) {
		var msg = "Post deleted successfully!";
		deleteAJournalEntry(id);
		flashMessage(msg);
	}
}

function updatePost(id) {
	var post = {};
	var msg = "Post updated successfully!";
	post.title = document.getElementById("mtitle").value;
	post.subTitle = document.getElementById("msubtitle").value;
	post.content = document.getElementById("mcontent").value;
	updateAJournalEntry(id, post);
	$(".modal").modal("toggle");
	console.log(msg);
	// Get flash message to run when successfully updated. NOTE - Run this in the ajax method
	flashMessage(msg);
}

// Flash a message onto the page
function flashMessage(msg) {
	$(".flashMsg").remove();
	$(".row").prepend(`
		  <div class="col-sm-12">
		  <div class="flashMsg alert alert-success alert-dismissible fade in" role="alert">
		  <button type="button" class="close" data-dismiss="alert" aria-label="Close">
		  <span aria-hidden="true">×</span></button> <strong>${msg}</strong></div></div>
      `);
}

// Reads the html text files and extracts the text
function redirectToFile(file) {
	var rawFile = new XMLHttpRequest();
	rawFile.open("GET", file);
	rawFile.onreadystatechange = function() {
		if (rawFile.readyState === 4) {
			if (rawFile.status === 200 || rawFile.status == 0) {
				var allText = rawFile.responseText;
				$("#mainBody").html(allText);
			}
		}
	};
	rawFile.send(null);
}

// Helper method to redirect to the registration form
function startRegistration() {
	redirectToFile(registerPath);
}

// Helper method to redirect to the login form
function loginNow() {
	redirectToFile(loginPath);
}

// Read the login information and send it to the login route
function login() {
	var usernameInput = $('input[name="username"]').val();
	var passwordInput = $('input[name="password"]').val();
	

	if (usernameInput && passwordInput) {
		var user = {
			username: usernameInput,
			password: passwordInput
		};

		loginCall(user);
	} else {
		alert("All fields must have a valid value.");
	}
}

// Read the registration information and pass it to the register endpoint
function registerNewUser() {
	var usernameInput = $('input[name="username"]').val();
	var passwordInput = $('input[name="password"]').val();

	if (usernameInput && passwordInput) {
		var user = {
			username: usernameInput,
			password: passwordInput
		};
		registerCall(user);
	} else {
		alert("All fields must have a valid value.");
	}
}


// Get all journals
function getAllJournalEntries() {
	$.ajax({
		url: `/api/entries`,
		type: "GET"
	})
		.done(data => {
			//async redirectToFile(journalPath);
			renderArticles(data);
			console.log(data);
		})
		.fail(err => {
			console.log(err);
			redirectToFile(loginPath);
		});
}

// The next ajax methods should have a fail that sends the user to the login screen.
// Create new post
function postAJournalEntry(entry) {
	$.ajax({
		url: `/api/entries`,
		type: "POST",
		dataType: "json",
		contentType: "application/json",
		data: JSON.stringify(entry)
	})
		.done(data => {
			getAllJournalEntries();
		})
		.fail(err => {
			redirectToFile(loginPath);
		});
}

// Delete post
function deleteAJournalEntry(id) {
	$.ajax({
		url: `/api/entries/${id}`,
		type: "DELETE"
	})
		.done(data => {
			getAllJournalEntries();
		})
		.fail(err => {
			redirectToFile(loginPath);
		});
}

// Update post
function updateAJournalEntry(id, post) {
	$.ajax({
		url: `/api/entries/${id}`,
		type: "PUT",
		dataType: "json",
		contentType: "application/json",
		data: JSON.stringify(post)
	})
		.done(data => {
			getAllJournalEntries();
		})
		.fail(err => {
			redirectToFile(loginPath);
		});
}

// Get one journal entry by id
function getAJournalEntry(id) {
	$.ajax({
		url: `/api/entries/${id}`,
		type: "GET"
	})
		.done(data => {
			editPost(data);
		})
		.fail(err => {
			redirectToFile(redirectToFile);
		});
}

// Calls the login endpoint of the api and redirects to the journal page if valid
function loginCall(user) {
	$.ajax({
		url: `/api/login`,
		type: "POST",
		dataType: "json",
		contentType: "application/json",
		data: JSON.stringify(user)
	}).done(data => {
		redirectToFile(journalPath);
		getAllJournalEntries();
	});
}

// Call the register endpoint, and redirect to login when it's valid
function registerCall(user) {
	$.ajax({
		url: `/api/register`,
		type: "POST",
		dataType: "json",
		contentType: "application/json",
		data: JSON.stringify(user)
	})
		.done(data => {
			redirectToFile(loginPath);
		})
		.fail(err => {
			console.log(err);
		});
}

function logout() {
	$.ajax({
		url: `/api/logout`,
		type: "GET"
	}).done(data => {
		redirectToFile(loginPath);
	});
}

// Globals
global.createPost = createPost;
global.editPost = editPost;
global.deletePost = deletePost;
global.updatePost = updatePost;
global.getAJournalEntry = getAJournalEntry;
global.startRegistration = startRegistration;
global.loginNow = loginNow;
global.login = login;
global.registerNewUser = registerNewUser;
global.logout = logout;
