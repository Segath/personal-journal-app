"use strict";

/**
 * Dependencies
 * @ignore
 */
const cwd = process.cwd();
const path = require("path");
const express = require("express");
const session = require("express-session");
const morgan = require("morgan");
const bodyParser = require("body-parser");
const cookieParser = require("cookie-parser");
const level = require("level");
const moment = require("moment");
const bcrypt = require("bcrypt");
const passport = require("passport");
const LocalStrategy = require("passport-local").Strategy;
const Sequelize = require("sequelize");

/**
 * Module Dependencies
 * @ignore
 */

/**
 * Start
 * @ignore
 */

// Open authdb
// A simple embedded key - value store using Google's leveldb; its like SQLite for JSON data.
const authdb = level(process.env.AUTH_DB || "authdb");

// express
const app = express();

// Connect to postgres database
const db = require("../models");

// Body parser middleware
app.use(bodyParser.urlencoded({ extended: false }));
app.use(bodyParser.json());

// Cookie parser middleware
app.use(cookieParser());

// In-memory session store config
app.use(
	session({
		resave: true,
		saveUninitialized: true,
		secret: "keyboard cat",
		cookie: {
			httpOnly: true,
			maxAge: 3600000 // 1 hour
		}
	})
);

// Setup request logger with predefined format
app.use(morgan("tiny"));

// Assign authdb to req and continue with next middleware
// Note: req is passed to each request the server receives
// IncomingRequest { } => ([err], req, res, next)
app.use((req, res, next) => {
	req.db = authdb;
	next();
});

// Passport setup
app.use(passport.initialize());
app.use(passport.session());

// Instruct passport how to convert a user object into the corresponding primary key
passport.serializeUser((user, done) => done(null, user.username || user));

// Instruct passport how to fetch a user object from the database
passport.deserializeUser((username, done) => done(null, { username }));

// Local authentication strategy setup
passport.use(
	new LocalStrategy(
		{
			// Field names to read off incoming requests
			usernameField: "username",
			passwordField: "password",

			// Remember authentication using sessions using previously configured express-session
			session: true,
			passReqToCallback: true


		},
		async (req, username, password, done) => {
			try {
				// Get stored bcrypt hash -- this will throw an error if not present.
				// Should find by some id?, might have multiple of the same email (username)
				const stored = await db.User.findOne({ where: { username } });

				if(!stored){
					return done(null, false, { message: "User does not exist." });
				}

				// Asynchronously compare the the hashed password
				const result = await new Promise((resolve, reject) => {
					bcrypt.compare(password, stored.password, (err, success) => {
						if (err) {
							return reject(err);
						}

						// Resolves true or false for comparison of the hash
						resolve(success);
					});
				});

				if (result) {
					return done(null, { username });
				} else {
					return done(null, false, { message: "Incorrect password." });
				}
			} catch (err) {
				// User doesn't exist
				if (err.notFound) {
					return done(null, false, { message: "User does not exist." });
				}

				// Something _really_ bad happened... if you ever get here then you messed up your code.
				return done(err);
			}
		}
	)
);

// Endpoints
app.post("/api/register", async (req, res, next) => {
	const { username, password } = req.body;

	const existing = await db.User.findOne({ where: { username } });
	if (existing) {
		// Again -- this is not a good idea from a security standpoint.
		return res.status(403).json({ status: "error", message: "User already exists." });
	}

	// Sequelize will not return an exception if it can't find an entry
	// User not found -- proceed with registration
	else {
		// Bcrypt config
		const saltRoundsString = process.env.SALT_ROUNDS;
		const saltRounds = saltRoundsString ? Number(saltRoundsString) : 10;

		// Create password hash
		// Thou shalt not store your passwords in plaintext.
		const hash = await bcrypt.hash(password, saltRounds);

		// Store in db -- asynchronous operation
		await db.User.create({ username, password: hash });

		// Notify the user of their success. Respond with 201 Created.
		// Note user is not logged in at this point.
		return res.status(201).json({ status: "ok", message: "User registered. Please log in." });
	}
});

// Passport login endpoint.
app.post("/api/login", passport.authenticate("local"), (req, res) =>
	res.status(200).json({ status: "ok", user: req.user })
);

// Clear user session.
app.get("/api/logout", (req, res) => {
	// Shortcut to clear user session.
	req.logout();
	res.status(200).json({ status: "ok" });
});

// Protected endpoint
app.get("/secret", (req, res) => {
	if (!req.isAuthenticated()) {
		return res.status(401).json({ status: "error" });
	}

	res.status(200).json({ status: "ok", secret: "hello, world!" });
});

// Start endpoints

// Might work now as I forgot that isAuthenticated is a method....

// Get all entries
app.get("/api/entries", (req, res) => {
	if (!req.isAuthenticated()) {
		return res.status(401).end();
	}
	// Note: userId column for entry require a string. My fault and will change it later
	db.User.findOne({ where: { username: req.user.username } }).then(user => {
		db.Entry.findAll({ where: { userId: user.id.toString() } }).then(entries => {
			res.status(200).json(entries.map(item => item.dataValues));
		});
	});
});

// Get one entry
app.get("/api/entries/:id", (req, res) => {
	if (!req.isAuthenticated()) {
		return res.status(401).end();
	}
	var entryId = req.params.id;
	db.Entry.findByPk(entryId)
		.then(entry => {
			if (entry) {
				return res.status(200).json(entry.dataValues);
			}

			return res.status(404).end();
		})
		.catch(err => {
			console.error(err);
			res.status(400).end();
		});
});

// Create a entry
app.post("/api/entries", (req, res) => {
	if (!req.isAuthenticated()) {
		return res.status(401).end();
	}
	var data = req.body;
	console.log("HERE!!! ", req.user.username);
	// Find the user in order to get it's id.
	db.User.findOne({ where: { username: req.user.username } }).then(user => {
		db.Entry.create({
			title: data.title,
			subTitle: data.subTitle,
			content: data.content,
			userId: user.id,
			createdDate: data.createdDate,
			modifiedDate: data.modifiedDate
		}).then(newEntry => {
			res.status(201).json(newEntry);
		});
	});
});

// Update an entry with whole object
app.put("/api/entries/:id", (req, res) => {
	if (!req.isAuthenticated()) {
		return res.status(401).end();
	}
	var data = req.body;
	var entryId = req.params.id;

	// Realized something here: This will look on the server's clock. So if it is located in
	// for example USA, then it will add the local american time.
	data.modifiedDate = moment().format("ddd H:mm, DD-MM-YYYY");

	db.Entry.findByPk(entryId)
		.then(Entry => {
			return Entry.set(data).save();
		})
		.then(updatedEntry => {
			res.status(200).json(updatedEntry);
		});
});

//Delete
app.delete("/api/entries/:id", (req, res) => {
	if (!req.isAuthenticated()) {
		return res.status(401).end();
	}
	db.Entry.findByPk(req.params.id)
		.then(user => {
			return user.destroy();
		})
		.then(() => {
			res.status(204).end();
		});
});

// End endpoints

if (process.env.NODE_ENV === "production") {
	// When in production mode: serve the built react app
	// When in development: the API is proxied from the create-react-app server.
	app.use(express.static(path.join(cwd, "app", "build")));
}

// Default error handler
// If you ever see this then you've done something really wrong.
app.use((err, req, res, next) => {
	console.error(err);

	// Send error message to client while not in production mode
	if (process.env.NODE_ENV !== "production") {
		return res.status(500).json({ status: "error", error: err });
	}

	res.status(500).json({ status: "error" });
});

// Start listening
const port = process.env.PORT || 3000;
app.listen(port, () => console.log(`Server running on port ${port}.`));

/**
 * Export
 * @ignore
 */
module.exports = app;
