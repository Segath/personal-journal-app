# Personal journal app
> 
> Front-end: A simple webpage which displays a list of journal entries from a database.
> Back-end: Express server with endpoints to connect to a database.
> Database: Docker postgres server
> 
## Install

```bash
$ npm install
$ docker pull postgres:11
```

## Startup

```bash
$ cd <project root folder>
$ docker-compose up
$ npm run dev (seperate CLI)
```

## Maintainers
- Jon Erik Selland
- Sondre Waage Tofte
- Shangavi Logeswaran

## License

MIT © 2019 Noroff Accelerate AS
