'use strict';

module.exports = {
  up: (queryInterface, Sequelize) => {
    return queryInterface.bulkInsert('Entries', [{
      title: 'Labjournal',
      subTitle: 'Create salt',
      content: 'Sea Salt',
      createdDate: "Mon 10:08, 18-09-2019",
      modifiedDate: "Wed 08:08, 18-09-2019",
      userId: 0,
      createdAt: new Date(),
      updatedAt: new Date()
    }], {});
  },

  down: (queryInterface, Sequelize) => {
    return queryInterface.bulkDelete('Entries', null, {});
  }
};
