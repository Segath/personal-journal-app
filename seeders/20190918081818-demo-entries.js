'use strict';

module.exports = {
  up: (queryInterface, Sequelize) => {
    return queryInterface.bulkInsert('Entries', [{
      title: 'Fitness Journal',
      subTitle: 'My Workout',
      content: 'Back & Bi',
      createdDate: "Wed 10:08, 18-09-2019",
      modifiedDate: "Wed 10:08, 18-09-2019",
      userId: 0,
      createdAt: new Date(),
      updatedAt: new Date()
    }], {});
  },

  down: (queryInterface, Sequelize) => {
    return queryInterface.bulkDelete('Entries', null, {});
  }
};
