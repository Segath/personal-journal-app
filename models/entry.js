"use strict";
module.exports = (sequelize, DataTypes) => {
	const Entry = sequelize.define(
		"Entry",
		{
			title: DataTypes.STRING,
			subTitle: DataTypes.STRING,
			content: DataTypes.STRING,
			createdDate: DataTypes.STRING,
			modifiedDate: DataTypes.STRING,
			userId: DataTypes.STRING,
			userId: DataTypes.STRING
		},
		{}
	);
	Entry.associate = function(models) {
		// associations can be defined here
	};
	return Entry;
};
